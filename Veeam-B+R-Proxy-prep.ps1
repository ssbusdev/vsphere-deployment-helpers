Start-Transcript C:\Deploy.log
Install-PackageProvider -Name NuGet -minimumVersion 2.8.5.201 -Force -Confirm:$false
Install-Module PSWindowsUpdate  -Force -Confirm:$false
Install-Module Firewall-Manager  -Force -Confirm:$false

function restore-rules() {
$RULES=@'
[
    {
        "Name":  "{53cba214-f0ca-465b-affa-05e3c6e5f1a2}",
        "DisplayName":  "BKP_SERVER-TCP-IN",
        "Description":  null,
        "Group":  null,
        "Enabled":  1,
        "Profile":  0,
        "Platform":  "",
        "Direction":  1,
        "Action":  2,
        "EdgeTraversalPolicy":  0,
        "LooseSourceMapping":  false,
        "LocalOnlyMapping":  false,
        "Owner":  null,
        "LocalAddress":  "Any",
        "RemoteAddress":  "10.28.25.101",
        "Protocol":  "TCP",
        "LocalPort":  "135,137-139,445",
        "RemotePort":  "Any",
        "IcmpType":  "Any",
        "DynamicTarget":  0,
        "Program":  "Any",
        "Package":  null,
        "Service":  "Any",
        "InterfaceAlias":  "Any",
        "InterfaceType":  0,
        "LocalUser":  "Any",
        "RemoteUser":  "Any",
        "RemoteMachine":  "Any",
        "Authentication":  0,
        "Encryption":  0,
        "OverrideBlockRules":  false
    },
    {
        "Name":  "{26a006bb-8ed8-48a3-b0e1-6cb49686bb04}",
        "DisplayName":  "BKP_gateway-TCP-IN",
        "Description":  null,
        "Group":  null,
        "Enabled":  1,
        "Profile":  0,
        "Platform":  "",
        "Direction":  1,
        "Action":  2,
        "EdgeTraversalPolicy":  0,
        "LooseSourceMapping":  false,
        "LocalOnlyMapping":  false,
        "Owner":  null,
        "LocalAddress":  "Any",
        "RemoteAddress":  "10.254.155.44",
        "Protocol":  "TCP",
        "LocalPort":  "135,137-139,445",
        "RemotePort":  "Any",
        "IcmpType":  "Any",
        "DynamicTarget":  0,
        "Program":  "Any",
        "Package":  null,
        "Service":  "Any",
        "InterfaceAlias":  "Any",
        "InterfaceType":  0,
        "LocalUser":  "Any",
        "RemoteUser":  "Any",
        "RemoteMachine":  "Any",
        "Authentication":  0,
        "Encryption":  0,
        "OverrideBlockRules":  false
    },
    {
        "Name":  "{77e9aad6-b1d0-45e2-93ff-3f6ea523838c}",
        "DisplayName":  "BKP_EMCDD-TCP-OUT",
        "Description":  null,
        "Group":  null,
        "Enabled":  1,
        "Profile":  0,
        "Platform":  "",
        "Direction":  2,
        "Action":  2,
        "EdgeTraversalPolicy":  0,
        "LooseSourceMapping":  false,
        "LocalOnlyMapping":  false,
        "Owner":  null,
        "LocalAddress":  "Any",
        "RemoteAddress":  "10.254.155.22",
        "Protocol":  "TCP",
        "LocalPort":  "135,137-139,445",
        "RemotePort":  "Any",
        "IcmpType":  "Any",
        "DynamicTarget":  0,
        "Program":  "Any",
        "Package":  null,
        "Service":  "Any",
        "InterfaceAlias":  "Any",
        "InterfaceType":  0,
        "LocalUser":  "Any",
        "RemoteUser":  "Any",
        "RemoteMachine":  "Any",
        "Authentication":  0,
        "Encryption":  0,
        "OverrideBlockRules":  false
    },
    {
        "Name":  "{043b2370-f2f9-4f26-99a3-6f9029812046}",
        "DisplayName":  "VCSA-TCP-OUT",
        "Description":  null,
        "Group":  null,
        "Enabled":  1,
        "Profile":  0,
        "Platform":  "",
        "Direction":  2,
        "Action":  2,
        "EdgeTraversalPolicy":  0,
        "LooseSourceMapping":  false,
        "LocalOnlyMapping":  false,
        "Owner":  null,
        "LocalAddress":  "Any",
        "RemoteAddress":  "192.168.31.1",
        "Protocol":  "TCP",
        "LocalPort":  "443,10443,902",
        "RemotePort":  "Any",
        "IcmpType":  "Any",
        "DynamicTarget":  0,
        "Program":  "Any",
        "Package":  null,
        "Service":  "Any",
        "InterfaceAlias":  "Any",
        "InterfaceType":  0,
        "LocalUser":  "Any",
        "RemoteUser":  "Any",
        "RemoteMachine":  "Any",
        "Authentication":  0,
        "Encryption":  0,
        "OverrideBlockRules":  false
    }
]
'@
$JSONPath = 'C:\Windows\temp\fwrules.json'
$RULES | Out-File $JSONPath
Import-FirewallRules -JSON $JSONPath
}


# Install latest updates
Get-WUlist -AcceptAll -Download -Install -Confirm:$False



# Remove default firewall rules
Get-NetFirewallRule | Remove-NetFirewallRule

# Set 'Block by default'
Get-NetFirewallProfile | Set-NetFirewallProfile -DefaultInboundAction Block -DefaultOutboundAction Block -LogMaxSizeKilobytes $((1024*10)) -LogAllowed 1 -LogBlocked 1   

restore-rules

Stop-Transcript